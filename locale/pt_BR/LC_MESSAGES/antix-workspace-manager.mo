��          �      ,      �     �     �     �  ?   �  5   �               5  #   ;  -   _  �   �       #   %  !   I     k     �  �  �     s     {  '   �  u   �  y         �  :   �     �  <   �  l      y  �     
  <   $
  9   a
  3   �
  0   �
                                                    	   
                       Apply Back Change Names Click Apply when desired names of workspaces have been entered. Click Done when desired number of workspaces was set. Done Enter the desired names: Leave Set the desired number of desktops: To change Workspace names click Change Names. Use mouse wheel or click on arrows. You can also enter a number by keyboard followed by enter key. The change takes place immediately. Workspace Number aWCS antiX workspace count switcher aWCS antiX workspace name changer antiX workspace count switcher antiX workspace name changer Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-28 15:48+0000
Last-Translator: marcelo cripe <marcelocripe@gmail.com>, 2023
Language-Team: Portuguese (Brazil) (https://www.transifex.com/anticapitalista/teams/10162/pt_BR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt_BR
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 Aplicar Voltar Alterar os Nomes das Áreas de Trabalho Quando você definir os nomes das áreas de\n trabalho virtuais desejados no antiX, clique\n no botão “Aplicar”. Quando você definir a quantidade de áreas de\n trabalho virtuais desejadas no antiX, clique no\n botão “Concluir”. Concluir Insira os nomes desejados das áreas de trabalho do antiX: Sair Defina a quantidade desejada de áreas de trabalho do antiX: Para alterar os nomes das áreas de trabalho, clique no botão “Alterar os Nomes das Áreas de Trabalho” Existem algumas formas diferentes para adicionar ou remover as áreas de trabalho virtuais no antiX:\n - Clique com o botão esquerdo na seta para cima para aumentar o valor ou na seta para baixo para diminuir o valor.\n - Utilize a roda do rato/mouse para cima para aumentar o valor ou para baixo para diminuir o valor.\n - Insira um número com o teclado e em seguida pressione a tecla Enter para confirmar o valor.\n - Pressione a tecla direcional para cima para aumentar o valor ou a tecla direcional para baixo para diminuir o valor.\n A adição ou a remoção das áreas de trabalho virtuais do antiX ocorre de forma imediata. Número da Área de Trabalho Gerenciador da Quantidade de Áreas de Trabalho para o antiX Gerenciador dos Nomes das Áreas de Trabalho para o antiX Alterar a quantidade de áreas de trabalho do antiX Alterar os nomes das áreas de trabalho do antiX 