��          �      ,      �     �     �     �  ?   �  5   �               5  #   ;  -   _  �   �       #   %  !   I     k     �  �  �     4     <     C  _   S  U   �     	       	   .  -   8  R   f  �   �     `  5     5   �  0   �  0                                                       	   
                       Apply Back Change Names Click Apply when desired names of workspaces have been entered. Click Done when desired number of workspaces was set. Done Enter the desired names: Leave Set the desired number of desktops: To change Workspace names click Change Names. Use mouse wheel or click on arrows. You can also enter a number by keyboard followed by enter key. The change takes place immediately. Workspace Number aWCS antiX workspace count switcher aWCS antiX workspace name changer antiX workspace count switcher antiX workspace name changer Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-28 15:48+0000
Last-Translator: Amigo, 2023
Language-Team: Spanish (https://www.transifex.com/anticapitalista/teams/10162/es/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es
Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 Aplicar Volver Cambiar nombres Haga clic en aplicar cuando se hayan ingresado los nombres deseados de los espacios de trabajo. Haga clic en listo cuando haya establecido el número deseado de espacios de trabajo. Listo Ingresar los nombres deseados: Abandonar Configurar el número deseado de escritorios: Para cambiar los nombres de los espacios de trabajo, haga clic en cambiar nombres. Use la rueda del ratón o haga clic en las flechas. También puede ingresar un número con el teclado seguido de la tecla Entrar. El cambio se produce inmediatamente. Número del espacio de trabajo Cambiador de conteo del espacio de trabajo aWCS antiX cambiador de nombre del espacio de trabajo aWCS antiX Cambiador de conteo del espacio de trabajo antiX cambiador de nombre del espacio de trabajo antiX 