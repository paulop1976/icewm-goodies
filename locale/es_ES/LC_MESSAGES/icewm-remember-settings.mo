��          �      ,      �  6   �      �  �   �     �     �     �  T   �  w   B     �     �     �  U   �     )  <   /  ;   l  	   �  �  �  E   e  2   �  J  �  
   )     4     :  h   ?  �   �     A	     M	     ^	  b   c	     �	  B   �	  I   
     Y
                                  	                
                            <b>Select a program. Store it's window properties.</b> Add/Remove IceWM Window Defaults Entries shown below are for the <b>$appclass</b> ($appname) window.\n\nAll options marked will be saved, all unmarked will be deleted.\n\n Note: Workspace number shown is the window's current workspace. \n 	Don't worry that it appears too low.\n\n Geometry HELP Layer Next time you launch the program, it will remember the window properties last saved. Save the <b>size and position</b>, <b>workspace</b> and <b>layer</b> of a window using the IceWM-remember-settings app. Select Select other Type Use the <b>Select other</b> option to select a different window/program to configure. Value What window configuration you want antiX to remember/forget? You can also delete this information unticking all options. workspace Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-28 15:49+0000
Last-Translator: Manuel <senpai99@hotmail.com>, 2023
Language-Team: Spanish (Spain) (https://www.transifex.com/anticapitalista/teams/10162/es_ES/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es_ES
Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 <b>Seleccionar un programa. Guarda las propiedades de la ventana.</b> Añadir/eliminar ventanas predeterminadas de IceWM Las entradas que se muestran a continuación corresponden a la ventana <b>$appclass</b> ($appname).\n\nTodas las opciones marcadas se guardarán, todas las no marcadas se borrarán.\n\n Nota: El número de espacio de trabajo mostrado es el espacio de trabajo de la ventana actual. \n 	No se preocupe si aparece demasiado bajo.\n\n Geometría AYUDA Capa La próxima vez que inicie el programa, éste recordará las propiedades de la última ventana guardada. Guardar el <b>tamaño y la posición</b>, <b>del espacio de trabajo</b> y la <b>capa</b> de una ventana mediante la aplicación IceWM-remember-settings. Seleccionar Seleccionar otro Tipo Utilice la opción <b>Seleccionar otro</b> para seleccionar otra ventana/programa para configurar. Valor ¿Qué configuración de ventanas desea que antiX recuerde/olvide? También puede eliminar esta información desmarcando todas las opciones. espacio de trabajo 