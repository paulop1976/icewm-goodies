��          �      ,      �     �     �     �  ?   �  5   �               5  #   ;  -   _  �   �       #   %  !   I     k     �  �  �     0     8     A  `   P  [   �               -  ,   4  E   a  �   �     U  (   k  (   �  #   �  #   �                                                    	   
                       Apply Back Change Names Click Apply when desired names of workspaces have been entered. Click Done when desired number of workspaces was set. Done Enter the desired names: Leave Set the desired number of desktops: To change Workspace names click Change Names. Use mouse wheel or click on arrows. You can also enter a number by keyboard followed by enter key. The change takes place immediately. Workspace Number aWCS antiX workspace count switcher aWCS antiX workspace name changer antiX workspace count switcher antiX workspace name changer Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-28 15:48+0000
Last-Translator: Besnik Bleta <besnik@programeshqip.org>, 2023
Language-Team: Albanian (https://www.transifex.com/anticapitalista/teams/10162/sq/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sq
Plural-Forms: nplurals=2; plural=(n != 1);
 Zbatoje Mbrapsht Ndryshoni Emra Kur të jenë dhënë emrat e dëshiruar të hapësirave të punës, klikoni mbi “Aplikoje”. Kur të jetë caktuar numri i dëshiruar i hapësirave të punës, klikoni mbi “U bë”. U bë Jepni emrat e dëshiruar: Mbylle Caktoni numrin e dëshiruar të dekstopëve: Që të ndryshohen emra Hapësirash Pune, klikoni mbi Ndryshoni Emra. Përdorni rrotëzën e miut, ose klikoni mbi shigjeta. Mundeni edhe të jepni një numër përmes tastierës, pasuar nga tasti Enter. Ndryshimet hyjnë në fuqi menjëherë. Numër Hapësire Pune Këmbyes numri hapësire pune antiX aWCS Ndryshues emri hapësire pune antiX aWCS Këmbyes numri hapësire pune antiX Ndryshues emri hapësire pune antiX 