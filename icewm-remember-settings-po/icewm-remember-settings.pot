# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-02-28 17:46+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: icewm-remember-settings:108
msgid "HELP"
msgstr ""

#: icewm-remember-settings:109
msgid ""
"Save the <b>size and position</b>, <b>workspace</b> and <b>layer</b> of a "
"window using the IceWM-remember-settings app."
msgstr ""

#: icewm-remember-settings:111
msgid ""
"Next time you launch the program, it will remember the window properties "
"last saved."
msgstr ""

#: icewm-remember-settings:113
msgid "You can also delete this information unticking all options."
msgstr ""

#: icewm-remember-settings:114
msgid ""
"Use the <b>Select other</b> option to select a different window/program to "
"configure."
msgstr ""

#: icewm-remember-settings:126 icewm-remember-settings:219
msgid "Add/Remove IceWM Window Defaults"
msgstr ""

#: icewm-remember-settings:127
msgid "<b>Select a program. Store it's window properties.</b>"
msgstr ""

#: icewm-remember-settings:128
msgid "What window configuration you want antiX to remember/forget?"
msgstr ""

#: icewm-remember-settings:220
#, sh-format
msgid ""
"Entries shown below are for the <b>$appclass</b> ($appname) window.\\n\\nAll "
"options marked will be saved, all unmarked will be deleted.\\n\\n Note: "
"Workspace number shown is the window's current workspace. \\n \tDon't worry "
"that it appears too low.\\n\\n"
msgstr ""

#: icewm-remember-settings:225
msgid "Select"
msgstr ""

#: icewm-remember-settings:225
msgid "Type"
msgstr ""

#: icewm-remember-settings:225
msgid "Value"
msgstr ""

#: icewm-remember-settings:226
msgid "Geometry"
msgstr ""

#: icewm-remember-settings:227
msgid "Layer"
msgstr ""

#: icewm-remember-settings:227
msgid "workspace"
msgstr ""

#: icewm-remember-settings:229
msgid "Select other"
msgstr ""
