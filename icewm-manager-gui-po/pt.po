# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# José Vieira <jvieira33@sapo.pt>, 2023
# Paulo C., 2023
# Hugo Carvalho <hugokarvalho@hotmail.com>, 2023
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-03-05 12:46+0200\n"
"PO-Revision-Date: 2023-02-28 15:49+0000\n"
"Last-Translator: Hugo Carvalho <hugokarvalho@hotmail.com>, 2023\n"
"Language-Team: Portuguese (https://www.transifex.com/anticapitalista/teams/10162/pt/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: pt\n"
"Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;\n"

#: icewm-manager-gui:9
msgid "Basic IceWM Settings Manager"
msgstr "Gestor de Definições Básicas do IceWM"

#: icewm-manager-gui:10
msgid ""
"Select what you want to change from the options below. If an option fails to"
" work, please try clicking it again."
msgstr ""
"Selecione o que quer alterar ao clicar nos botões abaixo. Se uma das opções "
"não funcionar imediatamente, tente clicar no mesmo botão novamente."

#: icewm-manager-gui:11
msgid "Clock"
msgstr "Relógio"

#: icewm-manager-gui:12
msgid "Show clock on/off"
msgstr "Mostrar/ocultar relógio"

#: icewm-manager-gui:13
msgid "12h/24h time"
msgstr "12h/24h"

#: icewm-manager-gui:14
msgid "Show seconds on clock on/off"
msgstr "Mostrar/ocultar segundos no relógio"

#: icewm-manager-gui:15
msgid "Show clock in LED on/off"
msgstr "Mostrar/ocultar relógio de LED"

#: icewm-manager-gui:16
msgid "System information shown in System Tray"
msgstr "Informação do sistema exibida na área de notificação"

#: icewm-manager-gui:17
msgid "Show Network information on System Tray on/off"
msgstr "Mostrar/ocultar informação de rede na área de notificação do sistema"

#: icewm-manager-gui:18
msgid "Show RAM information on System Tray on/off"
msgstr "Mostrar/ocultar informação de RAM na área de notificação do sistema"

#: icewm-manager-gui:19
msgid "Show CPU, RAM and SWAP  information on System Tray on/off"
msgstr ""
"Mostrar/ocultar informação de CPU, RAM e SWAP na área de notificação do "
"sistema"

#: icewm-manager-gui:20
msgid "Window Management"
msgstr "Gestão de Janelas"

#: icewm-manager-gui:21
msgid "Show Application icon on window title bar on/off"
msgstr "Mostrar/ocultar o ícone da aplicação na barra de título da janela"

#: icewm-manager-gui:22
msgid "Centre new large windows on/off"
msgstr "Ligar/desligar o centrar novas janelas grandes"

#: icewm-manager-gui:23
msgid "Show indication of position when moving windows  on/off"
msgstr "Mostrar/ocultar a indicação de posição ao mover janelas"

#: icewm-manager-gui:24
msgid "Click a Window to memorize its position"
msgstr "Clicar numa janela para memorizar a sua posição"

#: icewm-manager-gui:25
msgid "Auto Start"
msgstr "Início automático"

#: icewm-manager-gui:26
msgid "Add or Remove an application from IceWM start file"
msgstr "Adicionar ou Remover uma aplicação no arranque do IceWM"

#: icewm-manager-gui:27
msgid "Toolbar generic settings"
msgstr "Definições genéricas da Barra"

#: icewm-manager-gui:28
msgid "Auto-hide toolbar on/off"
msgstr "Ligar/desligar a barra de ferramentas de ocultação automática"

#: icewm-manager-gui:29
msgid "Double height toolbar on/off"
msgstr "Ligar/desligar barra com o dobro da altura"

#: icewm-manager-gui:30
msgid "Toolbar on top/bottom of screen"
msgstr "Barra em cima/em baixo "

#: icewm-manager-gui:31
msgid "Show window names on toolbar on/off"
msgstr "Mostrar/ocultar os nomes de janelas na barra de ferramentas"

#: icewm-manager-gui:32
msgid "Manage quick launch toolbar icons"
msgstr "Gerir ícones de inicio rápido da barra de ferramentas"

#: icewm-manager-gui:33
msgid "'Show Desktop' icon on/off"
msgstr "Mostrar/ocultar ícone 'Mostrar área de trabalho'"

#: icewm-manager-gui:34
msgid "Workspace indicator on/off"
msgstr "Ligar/desligar indicador de áreas de trabalho"

#: icewm-manager-gui:35
msgid "Workspace Manager"
msgstr "Gestor de áreas de trabalho"

#: icewm-manager-gui:36
msgid "Menu:"
msgstr "Menu:"

#: icewm-manager-gui:37
msgid "Manage Personal Menu entries"
msgstr "Gerir entradas Pessoais do menu"

#: icewm-manager-gui:38
msgid "Look and feel:"
msgstr "Aspeto e comportamento:"

#: icewm-manager-gui:39
msgid "Wallpaper"
msgstr "Imagem de fundo"

#: icewm-manager-gui:40
msgid "Appearance"
msgstr "Aparência"

#: icewm-manager-gui:41
msgid "Select IceWM Theme"
msgstr "Escolher Tema do IceWM"

#: icewm-manager-gui:42
msgid "Selected app icon to place on the (zzz) Desktop"
msgstr "Escolher ícone de app a colocar no ambiente de trabalho (zzz)"

#: icewm-manager-gui:43
msgid ""
"Do you want to add or to remove an entry to IceWm's startup? Current startup"
" file"
msgstr ""
"Deseja adicionar ou remover uma entrada ao arranque do IceWM? Ficheiro de "
"arranque atual"

#: icewm-manager-gui:44
msgid "Add application from a list"
msgstr "Adicionar uma aplicação a partir de uma lista"

#: icewm-manager-gui:45
msgid "Add a (manual) command"
msgstr "Adicionar um comando (manual)"

#: icewm-manager-gui:46
msgid "Remove an application"
msgstr "Remover uma aplicação"

#: icewm-manager-gui:47
#, sh-format
msgid ""
"The line was added to the $HOME/.icewm/startup file. It will start "
"automatically the next time you start  antiX using the IceWM desktop"
msgstr ""
"A linha foi adicionada ao ficheiro $HOME/.icewm/startup. Iniciará "
"automaticamente da próxima vez que iniciar o antiX usando o ambiente de "
"trabalho IceWM"

#: icewm-manager-gui:48
msgid "Enter command to be added to IceWM's startup file"
msgstr "Insira o comando a ser adicionado ao ficheiro de arranque do IceWM"

#: icewm-manager-gui:49
msgid ""
"Enter command you want to run at IceWM's startup. Note: an ampersand/& will "
"automatically be appended to the end of the command"
msgstr ""
"Insira o comando que deseja executar no arranque do IceWM. Nota: um 'e "
"comercial &' será automaticamente adicionado ao fim do comando"

#: icewm-manager-gui:51
msgid "Remove"
msgstr "Remover"

#: icewm-manager-gui:52
#, sh-format
msgid ""
"The line was removed from $HOME/.icewm/startup file. The related application"
" will no longer start automatically the next time you start  antiX using the"
" IceWM desktop"
msgstr ""
"A linha foi removida do ficheiro de arranque $HOME/.icewm/startup. A "
"aplicação relacionada não iniciará automaticamente na próxima vez que "
"iniciar o antiX usando o ambiente de trabalho IceWM"

#: icewm-manager-gui:53
msgid "IceWM Themes"
msgstr "Temas do IceWM"

#: icewm-manager-gui:54
msgid "Double click its name to select a new Theme:"
msgstr "Faça duplo clique no nome para selecionar um novo Tema:"

#: icewm-manager-gui:55
msgid "The current IceWM Theme is "
msgstr "O Tema atual do IceWM é "

#: icewm-manager-gui:56
msgid "Manage keyboard shortcuts"
msgstr "Atalhos do teclado"

#: icewm-manager-gui:57
msgid "Warning"
msgstr "Aviso"

#: icewm-manager-gui:58
msgid "This script is meant to be run only in an IceWM desktop"
msgstr ""
"Este script destina-se a ser executado apenas num ambiente de trabalho IceWM"

#: icewm-manager-gui:59
msgid "Reset to default contents"
msgstr "Restaurar o conteúdo predefinido"
