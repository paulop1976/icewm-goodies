# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# José Vieira <jvieira33@sapo.pt>, 2023
# marcelo cripe <marcelocripe@gmail.com>, 2023
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-02-19 16:07+0200\n"
"PO-Revision-Date: 2023-02-19 14:12+0000\n"
"Last-Translator: marcelo cripe <marcelocripe@gmail.com>, 2023\n"
"Language-Team: Portuguese (Brazil) (https://www.transifex.com/anticapitalista/teams/10162/pt_BR/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: pt_BR\n"
"Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;\n"

#: icewm-quick-personal-menu-manager:13
msgid "You are running an IceWM desktop"
msgstr ""
"Você está executando uma área de trabalho com o gerenciador e janelas IceWM"

#: icewm-quick-personal-menu-manager:15 icewm-quick-personal-menu-manager:119
msgid "Warning"
msgstr "Aviso"

#: icewm-quick-personal-menu-manager:15
msgid "This script is meant to be run only in an IceWM desktop"
msgstr ""
"Este conjunto de instruções (script) deve ser executado apenas em uma área "
"de trabalho com o gerenciador de janelas IceWM"

#: icewm-quick-personal-menu-manager:77 icewm-quick-personal-menu-manager:85
#: icewm-quick-personal-menu-manager:101 icewm-quick-personal-menu-manager:133
#: icewm-quick-personal-menu-manager:153 icewm-quick-personal-menu-manager:218
#: icewm-quick-personal-menu-manager:327
msgid "Personal Menu Ultra Fast Manager"
msgstr "Gerenciador do Menu Pessoal do IceWM"

#: icewm-quick-personal-menu-manager:77
msgid "Help::TXT"
msgstr "Ajuda::TXT"

#: icewm-quick-personal-menu-manager:77
msgid ""
"What is this?\\nThis utility adds and removes application icons from IceWm's"
" 'personal' list.\\nApplication icons are created from an application's "
".desktop file.\\nWhat are .desktop files?\\nUsually a .desktop file is "
"created during an application's installation process to allow the system "
"easy access to relevant information, such as the app's full name, commands "
"to be executed, icon to be used, where it should be placed in the OS menu, "
"etc.\\n.\\n Buttons:\\n 'ADD ICON' - select, from the list, application you "
"want to add to your 'personal' list and it instantly shows up in the menu or"
" submenu.\\nIf, for some reason, the correct icon for your application is "
"not found, a 'gears' icon will be used, so that you can still click it to "
"access the application.\\nYou can click the 'Advanced' button to manually "
"edit the relevant entry and change the application's icon.\\n'UNDO LAST "
"STEP' - every time an icon is added or removed from the toolbar, A backup "
"file is created. If you click this button, a restore is performed from that "
"backup file, without any confirmation.\\n'REMOVE ICON' - this shows a list "
"of all applications that have icons on your 'personal' list. Double left "
"click any application to remove its icon from the list\\n'ADVANCED' - allows"
" for editing the text configuration file that stores all of the entries in "
"your  'personal' list. Manually editing this file allows users to rearrange "
"the order of the icons and delete or add any icon. A brief explanation about"
" the inner workings of the text configuration file is displayed before the "
"file is opened for edition.\\n Warnings: only manually edit a configuration "
"file if you are sure of what you are doing! Always make a back up copy "
"before editing a configuration file!"
msgstr ""
"O que é o Gerenciador do Menu Pessoal do IceWM?\\nO Gerenciador do Menu "
"Pessoal do IceWM (nome original em idioma Inglês: ‘Personal Menu Ultra Fast "
"Manager’, sigla: PMUFM) é um programa utilitário GUI (Interface Gráfica do "
"Usuário) que permite adicionar ou remover itens ou ícones de atalho dos "
"programas aplicativos do menu pessoal na área de trabalho que tenha o IceWM "
"como seu gerenciador de janelas. O antiX possui a sua área de trabalho "
"padrão com o gerenciador de janelas IceWM.\\nOs ícones de atalho dos "
"aplicativos são criados a partir dos arquivos ‘.desktop’, onde cada "
"aplicativo possui o seu respectivo arquivo ‘.desktop’.\\nO que são os "
"arquivos ‘.desktop’?\\nNormalmente, um arquivo ‘.desktop’ é criado durante o"
" processo de instalação de um programa aplicativo para facilitar o acesso do"
" sistema operacional as informações relevantes, como por exemplo, o nome "
"completo do aplicativo, a descrição ou o comentário sobre o aplicativo, os "
"comandos a serem executados, a imagem que será utilizada no ícone de atalho,"
" em qual categoria deve ser alocado nos menus do sistema operacional, "
"etc.\\n.\\nComo adicionar os itens ou ícones de atalho no menu pessoal?\\n1 "
"- Inicie o Gerenciador do Menu Pessoal do IceWM, acesse o Menu> "
"Aplicativos/Aplicações> antiX> Gerenciador do Menu Pessoal do IceWM. "
"Dependendo da velocidade do seu computador, pode demorar alguns segundos "
"para carregar o programa, pois será listado todos os ícones dos aplicativos "
"instalados no antiX.\\n2 - Clique no botão ‘+ ADICIONAR O ÍCONE’.\\n3 - Será"
" exibida uma lista organizada em ordem alfabética, contendo quase todos os "
"aplicativos instalados pelo gerenciador de pacotes no seu sistema "
"operacional antiX. Selecione o aplicativo cujo ícone você quer ter no menu "
"pessoal e clique duas vezes com o botão esquerdo sobre o nome do aplicativo."
" Você também pode clicar uma vez com o botão esquerdo sobre o nome do "
"aplicativo e em seguida clicar com o botão esquerdo no botão ‘Adicionar o "
"Ícone do Aplicativo Selecionado’.\\n4 - O ícone é adicionado do menu pessoal"
" de forma instantânea e você pode adicionar quantos aplicativos desejar. "
"Quando você finalizar a personalização do seu menu pessoal, feche a janela "
"de seleção de ícones dos aplicativos e a janela principal do Gerenciador do "
"Menu Pessoal do IceWM.\\nSe, por algum motivo, a imagem do ícone correto do "
"aplicativo não for encontrada, a imagem de uma ‘engrenagem’ será utilizada, "
"para que você ainda possa clicar no menu para acessar o "
"aplicativo.\\n\\nComo remover os itens ou ícones de atalho no menu "
"pessoal?\\n1 - Inicie o Gerenciador do Menu Pessoal do IceWM, acesse o Menu>"
" Aplicativos> antix> Gerenciador do Menu Pessoal do IceWM. Dependendo da "
"velocidade do seu computador, pode demorar alguns segundos para carregar, "
"pois será listado todos os nomes dos ícones dos aplicativos disponíveis no "
"menu pessoal do IceWM.\\n2 - Clique no botão ‘- REMOVER O ÍCONE’.\\n3 - Será"
" exibida uma lista contendo todos os nomes dos ícones dos aplicativos que "
"são exibidos no menu pessoal do IceWM. Selecione o aplicativo cujo ícone "
"você deseja remover do menu pessoal e clique duas vezes com o botão esquerdo"
" sobre o nome do aplicativo. Você também pode clicar uma vez com o botão "
"esquerdo sobre o nome do aplicativo e em seguida clicar com o botão esquerdo"
" no botão ‘Remover o Ícone do Aplicativo Selecionado’.\\n4 - O ícone é "
"removido do menu pessoal de forma instantânea e você é enviado de volta para"
" a janela principal do Gerenciador do Menu Pessoal do IceWM.\\n5 - Se você "
"excluiu um ícone por engano, você pode clicar no botão ‘DESFAZER A ÚLTIMA "
"AÇÃO’ para o ícone que foi excluído por engano voltar a ser exibido no menu "
"pessoal.\\n\\nComo editar manualmente os itens ou ícones de atalho no menu "
"pessoal?\\n1 - Inicie o Gerenciador do Menu Pessoal do IceWM, acesse o Menu>"
" Aplicativos> antiX> Gerenciador do Menu Pessoal do IceWM.\\n2 - Clique no "
"botão ‘i CONFIGURAÇÕES AVANÇADAS’.\\n3 - Será exibida uma janela contendo um"
" breve tutorial sobre como editar manualmente o arquivo de configurações "
"‘personal’ que controla o menu pessoal.\\n4 - Clique no botão ‘Ok’, será "
"aberto o editor de texto para a edição manual do arquivo.\\nEditar "
"manualmente este arquivo possibilita ao usuário reorganizar a ordem dos "
"itens ou ícones de atalho do menu pessoal, adicionar ou mover ou excluir "
"qualquer ícone.\\nEsta é a maneira mais rica em recursos para editar os "
"ícones no menu pessoal, você pode comentar com ‘#’ os ícones indesejados "
"temporariamente, mover as linhas que se referem aos ícones, além de ser "
"muito mais rápido do que se você utilizar este utilitário GUI.\\nVocê também"
" pode adicionar manualmente os ícones que não tenham arquivos ‘.desktop’, "
"como, por exemplo, aplicativos executáveis do tipo ’AppImages’ ou arquivos "
"binários que você tenha no seu armazenamento interno (disco rígido, unidade "
"em estado sólido ou memória não volátil expressa), executar comandos "
"personalizados (por exemplo, ‘firefox https://www.youtube.com/’), conjuntos "
"de instruções (scripts) de inicialização, etc.\\nAvisos: Edite manualmente "
"um arquivo de configurações apenas se você souber exatamente o que está "
"fazendo! Sempre faça uma cópia de segurança do arquivo de configurações "
"antes de editar manualmente!"

#: icewm-quick-personal-menu-manager:85
msgid "Warning::TXT"
msgstr "Aviso::TXT"

#: icewm-quick-personal-menu-manager:85
msgid ""
"If you click to continue, the 'personal' configuration file will be opened for manual edition.\\n\n"
"How-to:\\nEach icon is identified by a line starting with 'prog' followed by the application name, icon and the application executable file.\\n Move, edit or delete the entire line referring to each entry.\\nNote: Lines starting with # are comments only and will be ignored.\\nThere can be empty lines.\\nAny changes appear instantly on the menu.\\nYou can undo the last change from UNDO LAST STEP button."
msgstr ""
"Para continuar, clique no botão ‘Ok’, o arquivo de configurações ‘personal’ "
"que controla o menu pessoal do gerenciador de janelas IceMW será aberto no "
"editor de texto para a edição manual.\\nProcedimentos para a edição "
"manual:\\nCada item ou ícone de atalho do menu pessoal é identificado por "
"uma linha começando com ‘prog’, seguido pelo nome do aplicativo, do caminho "
"onde a imagem do ícone está armazenada e do arquivo executável do "
"aplicativo.\\nAdicione, mova, edite ou exclua a linha inteira referente cada"
" entrada do item ou ícone de atalho do menu pessoal.\\nObservações: As "
"linhas que começam com um cerquilha ou octótropo ‘#’ são comentários e o "
"conteúdo posterior ao comentário não é exibido no menu pessoal.\\nO arquivo "
"‘personal’ pode ter linhas vazias ou em branco.\\nQualquer modificação feita"
" que for salva será exibida de forma instantânea no menu pessoal.\\nVocê "
"pode desfazer a última alteração clicando no botão ‘DESFAZER A ÚLTIMA AÇÃO’."

#: icewm-quick-personal-menu-manager:101
msgid "Double click any Application to remove its icon:"
msgstr ""
"Clique duas vezes com o botão esquerdo sobre o nome do aplicativo para "
"remover o seu ícone:"

#: icewm-quick-personal-menu-manager:101
msgid "Remove"
msgstr "Remover o Ícone do Aplicativo Selecionado"

#: icewm-quick-personal-menu-manager:112
msgid "file has something"
msgstr "O arquivo ‘personal’ que controla o menu pessoal não está vazio"

#: icewm-quick-personal-menu-manager:117
msgid "file is empty"
msgstr "O arquivo ‘personal’ que controla o menu pessoal está vazio"

#: icewm-quick-personal-menu-manager:119
msgid "No changes were made!\\nTIP: you can always try the Advanced buttton."
msgstr ""
"As modificações não foram feitas!\\nDica: Você pode tentar utilizar o botão "
"‘i CONFIGURAÇÕES AVANÇADAS’."

#: icewm-quick-personal-menu-manager:133
msgid "Double click any Application to move its icon:"
msgstr ""
"Clique duas vezes com o botão esquerdo sobre o nome do aplicativo para mover"
" o seu ícone:"

#: icewm-quick-personal-menu-manager:133
msgid "Move"
msgstr "Mover o Ícone do Aplicativo Selecionado"

#: icewm-quick-personal-menu-manager:143
msgid "nothing was selected"
msgstr "Nenhum programa aplicativo foi selecionado"

#: icewm-quick-personal-menu-manager:153
#, sh-format
msgid "Choose what do to with $EXEC icon"
msgstr "Escolha o que você quer fazer com o ícone $EXEC"

#: icewm-quick-personal-menu-manager:218
msgid "Add selected app's icon"
msgstr "Adicionar o Ícone do Aplicativo Selecionado"

#: icewm-quick-personal-menu-manager:331
msgid "HELP!help:FBTN"
msgstr "AJUDA!help:FBTN"

#: icewm-quick-personal-menu-manager:332
msgid "ADVANCED!help-hint:FBTN"
msgstr "CONFIGURAÇÕES AVANÇADAS!help-hint:FBTN"

#: icewm-quick-personal-menu-manager:333
msgid "ADD ICON!add:FBTN"
msgstr "ADICIONAR O ÍCONE!add:FBTN"

#: icewm-quick-personal-menu-manager:334
msgid "REMOVE ICON!remove:FBTN"
msgstr "REMOVER O ÍCONE!remove:FBTN"

#: icewm-quick-personal-menu-manager:335
msgid "UNDO LAST STEP!undo:FBTN"
msgstr "DESFAZER A ÚLTIMA AÇÃO!undo:FBTN"
