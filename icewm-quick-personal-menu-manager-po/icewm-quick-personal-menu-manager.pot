# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-02-28 17:44+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: icewm-quick-personal-menu-manager:13
msgid "You are running an IceWM desktop"
msgstr ""

#: icewm-quick-personal-menu-manager:15
#: icewm-quick-personal-menu-manager:119
msgid "Warning"
msgstr ""

#: icewm-quick-personal-menu-manager:15
msgid "This script is meant to be run only in an IceWM desktop"
msgstr ""

#: icewm-quick-personal-menu-manager:77
#: icewm-quick-personal-menu-manager:85
#: icewm-quick-personal-menu-manager:101
#: icewm-quick-personal-menu-manager:133
#: icewm-quick-personal-menu-manager:153
#: icewm-quick-personal-menu-manager:218
#: icewm-quick-personal-menu-manager:327
msgid "Personal Menu Ultra Fast Manager"
msgstr ""

#: icewm-quick-personal-menu-manager:77
msgid "Help::TXT"
msgstr ""

#: icewm-quick-personal-menu-manager:77
msgid ""
"What is this?\\nThis utility adds and removes application icons from IceWm's "
"'personal' list.\\nApplication icons are created from an application's ."
"desktop file.\\nWhat are .desktop files?\\nUsually a .desktop file is "
"created during an application's installation process to allow the system "
"easy access to relevant information, such as the app's full name, commands "
"to be executed, icon to be used, where it should be placed in the OS menu, "
"etc.\\n.\\n Buttons:\\n 'ADD ICON' - select, from the list, application you "
"want to add to your 'personal' list and it instantly shows up in the menu or "
"submenu.\\nIf, for some reason, the correct icon for your application is not "
"found, a 'gears' icon will be used, so that you can still click it to access "
"the application.\\nYou can click the 'Advanced' button to manually edit the "
"relevant entry and change the application's icon.\\n'UNDO LAST STEP' - every "
"time an icon is added or removed from the toolbar, A backup file is created. "
"If you click this button, a restore is performed from that backup file, "
"without any confirmation.\\n'REMOVE ICON' - this shows a list of all "
"applications that have icons on your 'personal' list. Double left click any "
"application to remove its icon from the list\\n'ADVANCED' - allows for "
"editing the text configuration file that stores all of the entries in your  "
"'personal' list. Manually editing this file allows users to rearrange the "
"order of the icons and delete or add any icon. A brief explanation about the "
"inner workings of the text configuration file is displayed before the file "
"is opened for edition.\\n Warnings: only manually edit a configuration file "
"if you are sure of what you are doing! Always make a back up copy before "
"editing a configuration file!"
msgstr ""

#: icewm-quick-personal-menu-manager:85
msgid "Warning::TXT"
msgstr ""

#: icewm-quick-personal-menu-manager:85
msgid ""
"If you click to continue, the 'personal' configuration file will be opened "
"for manual edition.\\n\n"
"How-to:\\nEach icon is identified by a line starting with 'prog' followed by "
"the application name, icon and the application executable file.\\n Move, "
"edit or delete the entire line referring to each entry.\\nNote: Lines "
"starting with # are comments only and will be ignored.\\nThere can be empty "
"lines.\\nAny changes appear instantly on the menu.\\nYou can undo the last "
"change from UNDO LAST STEP button."
msgstr ""

#: icewm-quick-personal-menu-manager:101
msgid "Double click any Application to remove its icon:"
msgstr ""

#: icewm-quick-personal-menu-manager:101
msgid "Remove"
msgstr ""

#: icewm-quick-personal-menu-manager:112
msgid "file has something"
msgstr ""

#: icewm-quick-personal-menu-manager:117
msgid "file is empty"
msgstr ""

#: icewm-quick-personal-menu-manager:119
msgid "No changes were made!\\nTIP: you can always try the Advanced buttton."
msgstr ""

#: icewm-quick-personal-menu-manager:133
msgid "Double click any Application to move its icon:"
msgstr ""

#: icewm-quick-personal-menu-manager:133
msgid "Move"
msgstr ""

#: icewm-quick-personal-menu-manager:143
msgid "nothing was selected"
msgstr ""

#: icewm-quick-personal-menu-manager:153
#, sh-format
msgid "Choose what do to with $EXEC icon"
msgstr ""

#: icewm-quick-personal-menu-manager:218
msgid "Add selected app's icon"
msgstr ""

#: icewm-quick-personal-menu-manager:331
msgid "HELP!help:FBTN"
msgstr ""

#: icewm-quick-personal-menu-manager:332
msgid "ADVANCED!help-hint:FBTN"
msgstr ""

#: icewm-quick-personal-menu-manager:333
msgid "ADD ICON!add:FBTN"
msgstr ""

#: icewm-quick-personal-menu-manager:334
msgid "REMOVE ICON!remove:FBTN"
msgstr ""

#: icewm-quick-personal-menu-manager:335
msgid "UNDO LAST STEP!undo:FBTN"
msgstr ""
