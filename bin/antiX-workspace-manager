#!/bin/bash
# -*- coding: utf-8 -*-
# GPL v.3
# antiX workspace count and name management
# by antiX community 2023
# ver. 0.1

TEXTDOMAINDIR=/usr/share/locale 
TEXTDOMAIN=antix-workspace-manager

temporaerdatei_01="/dev/shm/aWNC-$$01.tmp"

function aufräumen {
rm -f "$temporaerdatei_01" &>/dev/null
}
trap aufräumen EXIT

GtkdlgRahmen=false
#symbol_antiX=antixlogo  # GTK named symbol needed here
symbol_antiX="preferences-desktop-theme"
dx=$(wmctrl -d | tr -s " " | cut -d" " -f-4 | grep -F "*" | cut -d" " -f4 | cut -dx -f1)
dy=$(wmctrl -d | tr -s " " | cut -d" " -f-4 | grep -F "*" | cut -d" " -f4 | cut -dx -f2)

function Namen_ändern {
echo '
<window title="'$"aWCS antiX workspace name changer"'" decorated="$GtkdlgRahmen" icon-name="'$symbol_antiX'" skip_taskbar_hint="true" resizable="false">
  <vbox>
    <text use-markup="true">
      <label>"<b>'$"antiX workspace name changer"'</b>"</label>
    </text>
    <vbox>
      <text>
        <label>'$"Enter the desired names:"'</label>
      </text>' > "$temporaerdatei_01"

if [ $count -gt 10 ]; then
echo '        <hbox>
          <vbox>' >> "$temporaerdatei_01"
fi

for i in $(seq 0 $((count))) ; do
n="$(grep -F "#${i}" <<<"$(LANG=de_DE.UTF-8 icesh listWorkspaces)"| sed "s/..*: \x60\(.*\)\x60$/\1/")"
n="${n/</‹}"; n="${n/>/›}"; n="${n/\"/”}"; n="${n/ / }"  # we need to replace <>" to ‹›” to make gtkdialog and dash happy

if [ $i -eq 11 ] || [ $i -eq 22 ]; then
    echo '    </vbox>
          <vbox>' >> "$temporaerdatei_01"
fi
    echo '            <hbox>
              <text>
                <label>'$"Workspace Number"" ""$((i+1))"":"' </label>
              </text>
              <entry>' >> "$temporaerdatei_01"
if [ ! -z "$n" ]; then 
echo '                <default>'"$n"'</default>' >> "$temporaerdatei_01"
fi
echo '                <variable>Workspace_Name'$i'</variable>
              </entry>
            </hbox>' >> "$temporaerdatei_01"
done

if [ $count -gt 10 ]; then
echo '        </vbox>
          </hbox>' >> "$temporaerdatei_01"
fi

echo '    </vbox>
    <hbox space-expand="true" space-fill="true">
      <text><label>""</label></text>
      <button>
        <label>"'$"Back"'"</label>
      </button>
      <button>
        <label>"'$"Leave"'"</label>
      </button>
      <button tooltip-text="'$"Click Apply when desired names of workspaces have been entered."'">
        <label>"'$"Apply"'"</label>
      </button>
      <text><label>""</label></text>
    </hbox>
  </vbox>
</window>
' >> "$temporaerdatei_01"

cat -n "$temporaerdatei_01"

new_names=$(gtkdialog -f "$temporaerdatei_01" -G "+$(bc <<<"scale=0; $dx*0.9/10")+$(bc <<<"scale=0; $dy*8.5/10")")

done="EXIT=\""$"Apply"
back="EXIT=\""$"Back"
if [ $(grep "${done}" >/dev/null <<<"$new_names"; echo $?) -eq 0 ]; then 
   for i in $(seq 0 $((count))) ; do
       new_name="$(sed -n 's/Workspace_Name'$i'="\(.*\)"/\1/p' <<<"$new_names")"
       new_name="${new_name/\//⁄}"; new_name="${new_name/／/⁄}"  # The slash (/) and the fullwidth slash (／) character aren't displayed properly IceWM taskbar, even when the workspace name on console contains and shows these slashes properly. So we need to replace it here by a typographic similar character to see it in IceWM GUI.
       icesh setWorkspaceName $i "$new_name"
   done
   icesh restart
   sleep 0.5
   wmctrl -n $((count + 1))
   return 3
elif [ $(grep "${back}" >/dev/null <<<"$new_names"; echo $?) -eq 0 ]; then 
   return 2
fi
return 0
}

function Anzahl_ändern {
initial=$(wmctrl -d | wc -l)
export ANTIX_DESKTOP_COUNT_DIALOG='
<window title="'$"aWCS antiX workspace count switcher"'" decorated="$GtkdlgRahmen" icon-name="'$symbol_antiX'" skip_taskbar_hint="true" resizable="false">
  <vbox>
    <text use-markup="true">
      <label>"<b>'$"antiX workspace count switcher"'</b>"</label>
    </text>
    <hbox>
      <text>
        <label>'$"Set the desired number of desktops:"'</label>
      </text>
      <spinbutton range-min="1" range-max="32" range-step="1" range-value="'$initial'" tooltip-text="'$"Use mouse wheel or click on arrows. You can also enter a number by keyboard followed by enter key. The change takes place immediately."'">
	    <variable>ANZAHL</variable>
	    <action>"wmctrl -n $ANZAHL"</action>
      </spinbutton>
    </hbox>
    <hbox space-expand="true" space-fill="true">
      <text><label>""</label></text>
      <button tooltip-text="'$"To change Workspace names click Change Names."'">
        <label>"'$"Change Names"'"</label>
      </button>
      <button tooltip-text="'$"Click Done when desired number of workspaces was set."'">
        <label>"'$"Done"'"</label>
      </button>
      <text><label>""</label></text>
    </hbox>
  </vbox>
</window>
'
selection=$(gtkdialog --program=ANTIX_DESKTOP_COUNT_DIALOG -G "+$(bc <<<"scale=0; $dx*0.9/10")+$(bc <<<"scale=0; $dy*8.5/10")")

names="EXIT=\""$"Change Names"
export count=$(($(wmctrl -d | wc -l)-1))
if [ $(grep "${names}" >/dev/null <<<"$selection"; echo $?) -eq 0 ]; then return 3; fi
return 0
}

exitcode=2
while [ $exitcode -ne 0 ]; do
  if [ $exitcode -eq 3 ]; then
      Namen_ändern
      exitcode=$?
  elif [ $exitcode -eq 2 ]; then
      Anzahl_ändern
      exitcode=$?
  fi
done
exit 0
